
import java.util.Scanner;
import static org.junit.Assert.*;
import com.neoexpert.controller.*;
import com.neoexpert.nodes.*;
import com.neoexpert.Log;
import com.neoexpert.thingclient.*;
import org.junit.*;
import static org.junit.Assert.assertEquals;

public class TestThingClient {

	private static void _fail(String s) {
		Log.error(s);
		fail(s);
	}
	private static NodeClient nc;
	private static User u;
	private static long starttime;

	@BeforeClass
	public static void init() {
		starttime = System.currentTimeMillis();
		String username = "root";
		String password = "huhu";
		System.out.println("create NodeClient...");
		nc = new NodeClient("ws://localhost:8080/events");
		Log.ok();
		Log.log("logging in user=" + username + " pw=" + password + "...");
		u = nc.userLogin(username, password);
		Log.ok();
	}

	@Test
	public void testThingRead() {
		Log.info("READ TEST");
		System.out.println("get root Node...");
		Node n = nc.getRoot();
		assertNotNull(n);
		Log.ok();
		System.out.println("get Children-Iterator from root....");
		NodeIterator children = n.getChildren();
		Log.ok();
		children.addFieldToFilter("type", "core.Node");
		System.out.println("building the NodeIteraror...");
		children.build();
		Log.ok();
		Log.log("Iterating over Nodes...");

		while (children.hasNext()) {

			Node child = children.next();
			Log.log(child);

		}
		Log.ok();
	}

	@Test
	public void testAddChild() {
		Log.log("check if test_node exists...");
		Node n = nc.getRoot().getChild("test_node");
		if (n != null) {
			Log.log("test_node exists! deleting...");
			int deleted = nc.deleteNode(n, u);
			Log.log("deleted nodes: " + deleted);
			Log.ok();
		} else {
			Log.ok();
		}
		n = new Node(u.getID(), nc);
		n.setName("test_node");
		n = nc.getRoot().addChild(n, u);
		assertEquals(n, nc.getRoot().getChild("test_node"));
	}

}
