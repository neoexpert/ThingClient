/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neoexpert.controller;

import com.neoexpert.nodes.Node;
import com.neoexpert.nodes.User;
import com.neoexpert.thingclient.*;

import com.neoexpert.nodes.controller.Controller;
import com.neovisionaries.ws.client.WebSocket;
import com.neovisionaries.ws.client.WebSocketException;
import java.io.IOException;
import java.util.Iterator;
import java.util.Locale;
import org.json.JSONObject;

/**
 *
 * @author neoexpert
 */
public class WebSocketNodeIterator extends NodeIterator {

    private final Controller controller;

    private final JSONObject filter = new JSONObject();
    private WebSocket ws;
    private Lock lock;
    private WebSocketClient wsc;
    private boolean hasnext;
    private User u;

    NodeClient nc;
    public WebSocketNodeIterator(Controller c,String url) {
        this.controller = c;
        nc = new NodeClient(url);

        wsc = nc.getWebSocketClient();
    }

    @Override
    public NodeIterator addFieldToFilter(String name, Object value) {
        filter.put(name, value);
        return this;
    }

    @Override
    public NodeIterator build() throws RuntimeException {
        JSONObject find = new JSONObject();
        find.put("cmd", "findnew");
        find.put("filter", filter.toString());
        
        JSONObject first;
        u=controller.getUser();
        if(nc.userLogin(u.getName(),u.getPassword())!=null)
            first=wsc.send(find);
        else throw new RuntimeException("not logged in");
        hasnext=first.getBoolean("hasnext");

        return this;
    }

    @Override
    public NodeIterator addAttributeToFilter(String attribute, String value, Locale lang, boolean regex) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public NodeIterator addFieldToFilter(String name, int type, Object value, String op) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void close() {
				nc.close();
    }

    @Override
    public NodeIterator setPath(String path) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public NodeIterator addAttributeToExists(String name) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public NodeIterator setCallback(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getCallback() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public NodeIterator addAttributeToFilter(String key, Object value, boolean b) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public NodeIterator sort(JSONObject sort) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public NodeIterator sort(String sort, boolean desc) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean hasNext() {
        return hasnext;
    }

    @Override
    public Node next() {
        JSONObject next = wsc.send(new JSONObject().put("cmd", "next"));
        hasnext=next.getBoolean("hasnext");
        if(!hasnext)wsc.close();
        return new Node(next.getJSONObject("node"),controller);
    }

    @Override
    public Iterator<Node> iterator() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public User getUser(){
        return u;
    }

}
