package com.neoexpert.controller;

import java.util.Iterator;
import java.util.Locale;

import org.json.JSONObject;

import com.neoexpert.nodes.Node;

/**
 *
 * @author neoexpert
 */
public abstract class NodeIterator implements Iterator<Node>, Iterable<Node> {
        

    /**
     *
     * @param name
     * @param value
     * @return
     */
    public abstract NodeIterator addFieldToFilter(String name,  Object value);

    /**
     *
     * @return
     * @throws RuntimeException
     */
    public abstract NodeIterator build() throws RuntimeException;

    /**
     *
     * @param attribute
     * @param value
     * @param lang
     * @param regex
     * @return
     */
    public abstract NodeIterator addAttributeToFilter(String attribute, String value,Locale lang,boolean regex);

    /**
     *
     * @param name
     * @param type
     * @param value
     * @param op
     * @return
     */
    public abstract NodeIterator addFieldToFilter(String name, int type, Object value, String op);

    /**
     *
     */
    public abstract void close();

    /**
     *
     * @param path
     * @return
     */
    public abstract NodeIterator setPath(String path);

    /**
     *
     * @param name
     * @return
     */
    public abstract NodeIterator addAttributeToExists(String name);

    /**
     *
     * @param string
     * @return
     */
    public abstract NodeIterator setCallback(String string);

    /**
     *
     * @return
     */
    public abstract String getCallback();

    /**
     *
     * @param key
     * @param value
     * @param b
     * @return
     */
    public abstract NodeIterator addAttributeToFilter(String key, Object value, boolean b);

    /**
     *
     * @param sort
     * @return
     */
    public abstract NodeIterator sort(JSONObject sort);

    /**
     *
     * @param sort
     * @param desc
     * @return
     */
    public abstract NodeIterator sort(String sort, boolean desc);
}
