/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neoexpert.thingclient;

/**
 *
 * @author neoexpert
 */
import com.neoexpert.id.ObjectID;
import java.io.*;
import com.neovisionaries.ws.client.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;
import com.neoexpert.nodes.events.*;
import com.neoexpert.nodes.*;
import com.neoexpert.Log;
import com.neoexpert.nodes.controller.*;

public class WebSocketClient extends WebSocketAdapter {

	Controller c;
	public WebSocketClient(Controller c, String url){
    SERVER=url;
	this.c=c;
	}
    private static String SERVER;

    /**
     * The timeout value in milliseconds for socket connection.
     */
    private static final int TIMEOUT = 10000;
    private WebSocket ws;

    /**
     * The entry point of this command line application.
     *
     * @throws java.lang.Exception
     */
    public void main() throws Exception {
        // Connect to the echo server.
        connect();

        // The standard input via BufferedReader.
        BufferedReader in = getInput();

        // A text read from the standard input.
        String text;

        // Read lines until "exit" is entered.
        while ((text = in.readLine()) != null) {
            // If the input string is "exit".
            if (text.equals("exit")) {
                // Finish this application.
                break;
            }

            // Send the text to the server.
            ws.sendText(text);
        }

        // Close the WebSocket.
        ws.disconnect();
    }

    /**
     * Connect to the server.
     */
    public void connect() throws IOException, WebSocketException {
				Log.log("connecting to "+SERVER);
        ws = new WebSocketFactory()
                .setConnectionTimeout(TIMEOUT)
                .createSocket(SERVER)
                .addListener(this)
                .connect();

    }

    /**
     * Wrap the standard input with BufferedReader.
     */
    private static BufferedReader getInput() throws IOException {
        return new BufferedReader(new InputStreamReader(System.in));
    }


		@Override
		public
		void onDisconnected(WebSocket websocket,
        WebSocketFrame serverCloseFrame, WebSocketFrame clientCloseFrame,
        boolean closedByServer) throws Exception
		{
			Log.log("disconnected");
		}
    @Override
    public void onError(WebSocket websocket, WebSocketException cause) {
			Log.error(cause);
		}
    @Override
    public void onTextMessage(WebSocket websocket, String message) {
					Log.log(message,Log.DEBUG);
        JSONObject r = new JSONObject(message);
        String cmd = r.getString("cmd");
        switch (cmd) {
            case "result":
                Lock l = locks.remove(r.getInt("lock"));
                l.result = r.getJSONObject("result");
                synchronized (l) {
                    l.notify();
                }
                break;
	    case "event":
		processEvent(r);
		break;
        }
    }
    HashMap<ObjectID,Session> evs=new HashMap<>();
    public boolean addSession(ObjectID parent, Session ev){
 			JSONObject r = send(new JSONObject().put("cmd", "addSession").put("parent", parent.get()));
			evs.put(parent,ev);
			System.out.println(r.toString());

			return true;
    }
    public void processEvent(JSONObject e)	{
				System.out.println(e.toString());
				/*
   	int ID=e.getInt("nodeID"); 
	Session el=evs.get(ID);
	if(el!=null){
		Event ev=new Event(e,c);
		el.onEvent(ev);
	}*/
    }
    

    @Override
    public void onConnected(WebSocket websocket, Map<String, List<String>> headers) throws Exception {
        super.onConnected(websocket, headers); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onConnectError(WebSocket websocket, WebSocketException exception) throws Exception {
        super.onConnectError(websocket, exception); //To change body of generated methods, choose Tools | Templates.
				Log.logGreen("connected");
    }

    HashMap<Integer, Lock> locks = new HashMap<>();
    private int lockid = 0;

    private Lock addLock() {
        lockid++;
        Lock l = new Lock(lockid);
        locks.put(lockid, l);
        return l;

    }


    public JSONObject send(JSONObject jo) {
        Lock l = addLock();
        jo.put("lock", l.getID());
        send(jo.toString());
        synchronized (l) {
            try {
                l.wait(TIMEOUT);
            } catch (InterruptedException ex) {
                Log.error(ex);
            }
        }
        if(l.result==null) throw new RuntimeException("TIMEOUT");
        return l.result;
    }

    public void send(String message) {
        ws.sendText(message);
    }

    public void close() {
				Log.log("closed connection to "+SERVER);
        ws.disconnect();
    }
}
