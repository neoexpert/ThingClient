package com.neoexpert.thingclient;

import com.neoexpert.Log;
import com.neoexpert.controller.WebSocketNodeIterator;
import com.neoexpert.nodes.Node;
import com.neoexpert.nodes.User;
import com.neoexpert.nodes.attribute.Attribute;
import com.neoexpert.nodes.controller.Controller;
import com.neoexpert.nodes.events.*;
import com.neoexpert.controller.NodeIterator;
import com.neoexpert.id.ObjectID;
import com.neovisionaries.ws.client.WebSocketException;
import java.io.IOException;
import java.util.*;
import org.json.*;

public class NodeClient extends Controller 
{
	public static String url="ws://localhost:8080/events";
	public static void main(String[] args) throws Exception {
		String username = null;
		String password = null;
		for (int i = 0; i < args.length; i++) {
			switch (args[i]) {
				case "-h":
					if (args.length > i) {
						url = args[i + 1];
					}
					break;
				case "-u":
					if (args.length > i) {
						username = args[i + 1];
					}
					break;
				case "-p":
					if (args.length > i + 1) {
						password = args[i + 1];
					} else {
						System.out.print("password:");
						password = new String(System.console().readPassword());
					}
					break;
			}
		}
		NodeClient nc = new NodeClient(url);
		User u = nc.userLogin(username, password);

		Node n = nc.getRoot();
		Session s=new Session(){
			@Override
			public void onEvent(Event e){
				System.out.println(e.toString());
			}
		};
		nc.addSession(n,s);
		printRecursive(n);

		System.out.println("done...");
	}

	private static void printRecursive(Node n) {
		Stack<Node> stack = new Stack<>();
		stack.push(n);
		while (!stack.isEmpty()) {
			n = stack.pop();
			System.out.println(n.toString());
			NodeIterator children = n.getChildren();
			children.build();

			while (children.hasNext()) {
				try {

					Node child = children.next();

					stack.push(child);
				} catch (Exception e) {

					System.out.println(e.toString());
					Log.error(e);
					break;

				}

			}
			children.close();
		}
	}
	private final WebSocketClient wsc;
	private User u;

	public NodeClient(String Url) throws RuntimeException {
		super();
		url=Url;
		
		try {
			wsc = new WebSocketClient(this,url);
			wsc.connect();
		} catch (IOException | WebSocketException ex) {
						ex.printStackTrace();
			throw new RuntimeException("could not connect");
		}

	}

	@Override
	public Node addNode(Node n, User u) throws RuntimeException {
		JSONObject r = wsc.send(new JSONObject().put("cmd", "addNode").put("node", n.toJSON()));
		if(r.has("error")){
			throw new RuntimeException(r.getString("error"));	
		}
		return new Node(r, this);
	}

	@Override
	public boolean addSession(Node n, Session ev) {
		return wsc.addSession(n.getID(), ev);
	}

	@Override
	public Event addEvent(Event e, User u) throws RuntimeException {
		// TODO: Implement this method
		return null;
	}

	@Override
	public void addToArchive(Node cat) throws RuntimeException {
		// TODO: Implement this method
	}

	@Override
	public void addToArchive(String contaienr, ObjectID id) throws RuntimeException {
		// TODO: Implement this method
	}

	@Override
	public User addUser(String name, String password) throws RuntimeException {
		// TODO: Implement this method
		return null;
	}

	@Override
	public boolean nodeExists(ObjectID id, String name) throws RuntimeException {
		// TODO: Implement this method
		return false;
	}

	@Override
	public void copy(Node cat, Node container, User u) throws RuntimeException {
		// TODO: Implement this method
	}

	@Override
	public void integrate(Node cat, Node container, User u) throws RuntimeException {
		// TODO: Implement this method
	}

	@Override
	public void decrementOrderID(Node n, User u) throws RuntimeException {
		// TODO: Implement this method
	}

	@Override
	public int delete(ObjectID childID, User u) throws RuntimeException {
		// TODO: Implement this method
		return 0;
	}

	@Override
	public boolean deletePicture(String path, ObjectID id, User u) throws RuntimeException {
		// TODO: Implement this method
		return false;
	}

	@Override
	public Iterator<Node> getArchive(String contaienr, ObjectID id) throws RuntimeException {
		// TODO: Implement this method
		return null;
	}

	@Override
	public LinkedHashMap<String, Attribute> getAttributes(ObjectID parent, String attr_type) throws RuntimeException {
		// TODO: Implement this method
		return null;
	}

	@Override
	public LinkedHashMap<String, Attribute> getAttributes(ObjectID id) throws RuntimeException {
		// TODO: Implement this method
		return null;
	}

	@Override
	public void close() {
			wsc.close();
	}

	@Override
	public NodeIterator find(String name, String filter, boolean allfields) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public long getChildrenCount(ObjectID id, Class<?> type) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public long getChildrenCount(ObjectID iD) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public Node getNode(ObjectID id) {
		JSONObject r = wsc.send(new JSONObject().put("cmd", "getNode").put("_id", id.get()));
		if(r.has("error"))return null;

		return new Node(r, this);
	}

	@Override
	public Node getChild(ObjectID parent, String name) {
		JSONObject r = wsc.send(new JSONObject()
				.put("cmd", "getChild")
				.put("parent", parent.get())
				.put("name", name));
		if(r.has("error"))return null;

		return new Node(r, this);
	}

	@Override
	public Node getNode(ObjectID parentID, String name, Class<?> type) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public Node getNode(ObjectID parentID, Class<?> type, String name) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public Node getNode(String name) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public Node getNode(ObjectID parent, ObjectID id) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public Node getNodeByUrl(Node root, String caption, Locale lang) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public NodeIterator getNodeByConfiguration(Node node, JSONObject confs) throws JSONException {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public Node getNodeLang(ObjectID id, Class<?> type, String attrname, Locale lang) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}


	@Override
	public ObjectID getRootID() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public void incrementOrderID(Node n, User u) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public Node moveNode(User u, ObjectID id, ObjectID parent) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public void moveNode(Node cat, Node container, User u) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public void refreshUpdated(ObjectID id) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public void setup(User u) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public Node updateNode(User u, Node cat) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public User userLogin(String username, String password) {
		JSONObject jo = wsc.send(new JSONObject().put("cmd", "login").put("username", username).put("password", password));
		u = new User(jo.getJSONObject("user"), this);
		u.setPassword(password);
		return u;
	}

	@Override
	public boolean isSuperRoot() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public Locale getLang() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public int deleteNode(Node n, User u) {
		JSONObject r = wsc.send(new JSONObject()
			.put("cmd", "delNode")
			.put("_id", n.getID().get()));
		if(r.has("error")){
			throw new RuntimeException(r.getString("error"));	
		}
		if(r.has("deleted"))
			return r.getInt("deleted");
		return 0;
	}

	@Override
	public NodeIterator getAllChildren(Node node, String name) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public Node copyNode(User u, Node n, ObjectID newparent) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public User getUser(String name) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public int getNextCID(Node node) {
		return 0;
	}

	@Override
	public Node copyNode(User u, Node child_copy, Node parentnode) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public NodeIterator find() {
		return new WebSocketNodeIterator(this,url);
	}

	@Override
	public void deleteAttr(Attribute attribute) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public void accessNode(Node node) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	protected Node createRoot() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public Node addToSet(Node node, String name, String value, User u) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public Node removeFromSet(Node node, String name, String value, User u) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public Node addToList(Node node, String name, String value, User u) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public Node removeAllFromList(Node node, String name, String value, User u) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public void deleteAttr(Node node, String name, User u) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public void addAttr(Node node, String name, User u) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public void addToMap(Node parent, Attribute attr, String key, String value, Locale locale) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public Object getConnection() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	public WebSocketClient getWebSocketClient() {
		return wsc;
	}

	@Override
	public User getUser() {
		return u;
	}

}
