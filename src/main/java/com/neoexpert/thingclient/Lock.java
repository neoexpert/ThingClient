/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neoexpert.thingclient;

import org.json.JSONObject;

/**
 *
 * @author neoexpert
 */
public class Lock {

    public JSONObject result;
    private final int lockid;

    public Lock(int lockid) {
        this.lockid = lockid;
    }

    public int getID() {
        return lockid;
    }
}
