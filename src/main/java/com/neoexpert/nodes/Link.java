package com.neoexpert.nodes;

import com.neoexpert.id.ObjectID;
import java.util.Locale;

/**
 *
 * @author neoexpert
 */
public class Link extends Node {

    /**
     *
     * @param c
     */
    public Link(Node c) {
        super(c);
    }

    @Override
    public String getFrontLink(Locale lang) {
        return getTarget().getFrontLink(lang);
    }

    @Override
    public String getBackLink(Locale lang) {
        Node cat = getTarget();

        if (cat == null) {
            return super.getBackLink(lang) + " => undefined";
        } else {
            return super.getBackLink(lang) + " => " + cat.getBackLink(lang);
        }
    }

    /**
     *
     * @return @throws NumberFormatException
     */
    public Node getTarget() throws NumberFormatException {
        return c.getNode(new ObjectID(doc.getString("target")));
    }

    @Override
    public Node getChild(String name) {
        return getTarget().getChild(name);
    }

}
