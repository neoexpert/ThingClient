package com.neoexpert.nodes.events;
import com.neoexpert.nodes.User;
import com.neoexpert.id.ObjectID;
import org.json.JSONObject;


import com.neoexpert.nodes.Node;
import com.neoexpert.nodes.controller.Controller;

/**
 *
 * @author neoexpert
 */
public class Event extends Node {

    String type = "";

    /**
     *
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }
    private Node n;
    private String oldvalue;
    private String newvalue;

    /**
     *
     * @param n
     * @param type
     * @param con
     * @param oldvalue
     * @param newvalue
     */
    public Event(Node n,User u, String type, Controller con, String oldvalue, String newvalue) {
        super(u.getID(), con);
        this.n = n;
        this.type = type;
        //this.eventName=eventName;
        this.oldvalue = oldvalue;
        this.newvalue = newvalue;
    }

    /**
     *
     * @param cat
     */
    public Event(Node cat) {
        super(cat);
    }

    public Event(JSONObject e,Controller c) {
	super(e,c);
    }


    /**
     *
     * @return
     */
    public Node getNode() {
        return n;
    }

    /**
     *
     * @return
     */
  

    /**
     *
     * @return
     */
    @Override
    public String getPreviousValue() {
        return oldvalue;
    }

    /**
     *
     * @return
     */
    @Override
    public String getNewValue() {
        return newvalue;
    }

    /**
     *
     * @return
     */
    public Object getValue() {
        return doc.get("value");
    }

    /**
     *
     * @param value
     */
    public void setValue(Object value) {
        doc.put("value", value);
    }

}
