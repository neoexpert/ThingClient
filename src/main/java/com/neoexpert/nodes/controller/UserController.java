package com.neoexpert.nodes.controller;

import com.neoexpert.nodes.User;

/**
 *
 * @author neoexpert
 */
public interface UserController {

    /**
     *
     * @param email
     * @param password
     * @return
     * @throws RuntimeException
     */
    User addUser(String email, String password) throws RuntimeException;

    /**
     *
     * @param userEMail
     * @param password
     * @return
     * @throws RuntimeException
     */
    User userLogin(String userEMail, String password) throws RuntimeException;

    /**
     *
     */
    void close();

    /**
     *
     * @param name
     * @return
     * @throws RuntimeException
     */
    User getUser(String name)throws RuntimeException;
}
