package com.neoexpert.nodes.controller;

import com.neoexpert.nodes.types.*;
import com.neoexpert.nodes.*;
import com.neoexpert.nodes.attribute.*;
import com.neoexpert.nodes.events.*;
import com.neoexpert.nodes.templates.*;
import java.io.IOException;
import java.io.Writer;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import com.neoexpert.controller.NodeIterator;
import com.neoexpert.id.ObjectID;

/**
 *
 * @author neoexpert
 */
public abstract class Controller {

	protected ObjectID rootID=new ObjectID("ROOT");
    public Settings getSesstings() {
        return (Settings)getNode("SETTINGS");
    }

    public abstract User getUser();

   
    /**
     *
     */
    public static class NodeExistsException extends RuntimeException {

        private final Exception e;

        /**
         *
         * @param e
         */
        public NodeExistsException(Exception e) {
            this.e = e;
        }

        /**
         *
         * @return
         */
        public Exception getReason() {
            return e;
        }

    }

    /**
     *
     * @return
     */
    public Types getTypes() {
        Node types;
        types = getNode("TYPES");
        if (types instanceof Types) {
            return (Types) types;
        }
        return (Types) getNode(rootID).getChild("TYPES");
    }

   
    /**
     *
     * @return
     */
    public Node getMailSettings() {
        return getNode("SETTINGS").getChild("EMAIL");
    }

    /**
     *
     * @param cat
     * @param u
     * @return
     */
    public abstract Node addNode(Node n, User u);

    public abstract boolean addSession(Node n, Session ev);


    /**
     *
     * @param e
     * @param u
     * @return
     */
    public abstract Event addEvent(Event e, User u);

    /**
     *
     * @param cat
     */
    public abstract void addToArchive(Node cat);

    /**
     *
     * @param contaienr
     * @param id
     */
    public abstract void addToArchive(String contaienr, ObjectID id);

    /**
     *
     * @param name
     * @param password
     * @return
     */
    public abstract User addUser(String name, String password);

    /**
     *
     */
    public abstract void close();

    /**
     *
     * @param cat
     * @param container
     * @param u
     */
    public abstract void copy(Node cat, Node container, User u);

    /**
     *
     * @param cat
     * @param container
     * @param u
     */
    public abstract void integrate(Node cat, Node container, User u);

    /**
     *
     * @param n
     * @param u
     */
    public abstract void decrementOrderID(Node n, User u);

    /**
     *
     * @param childID
     * @param u
     * @return
     */
    public abstract int delete(ObjectID childID, User u);

    /**
     *
     * @param path
     * @param id
     * @param u
     * @return
     */
    public abstract boolean deletePicture(String path, ObjectID id, User u);

    /**
     *
     * @param contaienr
     * @param id
     * @return
     */
    public abstract Iterator<Node> getArchive(String contaienr, ObjectID id);

    /**
     *
     * @param parent
     * @param attr_type
     * @return
     */
    public abstract LinkedHashMap<String, Attribute> getAttributes(ObjectID parent, String attr_type);

    /**
     *
     * @param id
     * @return
     */
    public abstract LinkedHashMap<String, Attribute> getAttributes(ObjectID id);

    /**
     *
     * @param name
     * @param filter
     * @param allfields
     * @return
     */
    public abstract NodeIterator find(String name, String filter, boolean allfields);

    /**
     *
     * @param id
     * @param type
     * @return
     */
    public abstract long getChildrenCount(ObjectID id, Class<?> type);

    public abstract long getChildrenCount(ObjectID iD);
    /**
     *
     * @param id
     * @return
     */
    public abstract Node getNode(ObjectID id);

    /**
     *
     * @param parentID
     * @param name
     * @return
     */
    public abstract Node getChild(ObjectID parentID, String name);

    /**
     *
     * @param parentID
     * @param name
     * @param type
     * @return
     */
    public abstract Node getNode(ObjectID parentID, String name, Class<?> type);

    /**
     *
     * @param parentID
     * @param type
     * @param name
     * @return
     */
    public abstract Node getNode(ObjectID parentID, Class<?> type, String name);

    /**
     *
     * @param name
     * @return
     */
    public abstract Node getNode(String name);

    /**
     *
     * @param parent
     * @param id
     * @return
     */
    public abstract Node getNode(ObjectID parent, ObjectID id);

    /**
     *
     * @param root
     * @param caption
     * @param lang
     * @return
     */
    public abstract Node getNodeByUrl(Node root, String caption, Locale lang);

    /**
     *
     * @param node
     * @param confs
     * @return
     * @throws JSONException
     */
    public abstract NodeIterator getNodeByConfiguration(Node node, JSONObject confs) throws JSONException;

    /**
     *
     * @param id
     * @param type
     * @param attrname
     * @param lang
     * @return
     */
    public abstract Node getNodeLang(ObjectID id, Class<?> type, String attrname, Locale lang);

    

	public Node getRoot() {
		return getNode(rootID);
	}

    /**
     *
     * @return
     */
    public abstract ObjectID getRootID();

    /**
     *
     * @param n
     * @param u
     */
    public abstract void incrementOrderID(Node n, User u);

    /**
     *
     * @param u
     * @param id
     * @param parent
     * @return
     */
    public abstract Node moveNode(User u, ObjectID id, ObjectID parent);

    /**
     *
     * @param cat
     * @param container
     * @param u
     */
    public abstract void moveNode(Node cat, Node container, User u);

    /**
     *
     * @param id
     */
    public abstract void refreshUpdated(ObjectID id);

    /**
     *
     * @param u
     */
    public abstract void setup(User u);

    /**
     *
     * @param u
     * @param cat
     * @return
     */
    public abstract Node updateNode(User u, Node cat);

    /**
     *
     * @param username
     * @param password
     * @return
     */
    public abstract User userLogin(String username, String password);

    /**
     *
     * @return
     */
    public abstract boolean isSuperRoot();

    /**
     *
     * @return
     */
    public abstract Locale getLang();

    /**
     *
     * @param n
     * @param u
     * @return
     */
    public abstract int deleteNode(Node n, User u);

    /**
     *
     * @param node
     * @param name
     * @return
     */
    public abstract NodeIterator getAllChildren(Node node, String name);

    /**
     *
     * @param u
     * @param n
     * @param newparent
     * @return
     */
    public abstract Node copyNode(User u, Node n, ObjectID newparent);

    /**
     *
     * @param name
     * @return
     */
    public abstract User getUser(String name);

    /**
     *
     * @param node
     * @return
     */
    public abstract int getNextCID(Node node);

    /**
     *
     * @param u
     * @param child_copy
     * @param parentnode
     * @return
     */
    public abstract Node copyNode(User u, Node child_copy, Node parentnode);

    /**
     *
     * @return
     */
    public abstract NodeIterator find();

    /**
     *
     * @param attribute
     */
    public abstract void deleteAttr(Attribute attribute);

    /**
     *
     * @param parent
     * @param name
     * @return
     */
    public abstract boolean nodeExists(ObjectID parent, String name);

    /**
     *
     * @param node
     */
    public abstract void accessNode(Node node);

    /**
     *
     * @param path
     * @return
     */
    public Node getNodeByPath(String path) {
        String[] names = path.split("/");
        Node n = getNode("CATEGORIES");
        for (String name : names) {
            n = n.getChild(name);
        }
        return n;
    }

    /**
     *
     * @return
     */
    protected abstract Node createRoot();

    /**
     *
     * @return
     */
    

    /**
     *
     * @param node
     * @param name
     * @param value
     * @param u
     * @return
     */
    public abstract Node addToSet(Node node, String name, String value, User u);

    /**
     *
     * @param node
     * @param name
     * @param value
     * @param u
     * @return
     */
    public abstract Node removeFromSet(Node node, String name, String value, User u);

    /**
     *
     * @param node
     * @param name
     * @param value
     * @param u
     * @return
     */
    public abstract Node addToList(Node node, String name, String value, User u);

    /**
     *
     * @param node
     * @param name
     * @param value
     * @param u
     * @return
     */
    public abstract Node removeAllFromList(Node node, String name, String value, User u);

    /**
     *
     * @param node
     * @param name
     * @param u
     */
    public abstract void deleteAttr(Node node, String name, User u);

    /**
     *
     * @param node
     * @param name
     * @param u
     */
    public abstract void addAttr(Node node, String name, User u);

    /**
     *
     * @param parent
     * @param attr
     * @param key
     * @param value
     * @param locale
     */
    public abstract void addToMap(Node parent, Attribute attr, String key, String value, Locale locale);


    public abstract Object getConnection();

}
