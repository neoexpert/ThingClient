package com.neoexpert.nodes;

import com.neoexpert.nodes.types.Types;

/**
 *
 * @author neoexpert
 */
public class Domain extends Node {
	static{
		addChildType(Domain.class,Users.class);
		addChildType(Domain.class,Node.class);
		addChildType(Domain.class,Types.class);

	}

    /**
     *
     * @param c
     */
    public Domain(Node c) {
		super(c);
	}
}
