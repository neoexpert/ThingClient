package com.neoexpert.nodes;
import com.neoexpert.id.ObjectID;

import java.util.List;

import com.neoexpert.nodes.controller.Controller;
import org.json.JSONObject;

/**
 * Ein Benutzer ist ein Node der Nodes "USERS".
 *
 * @author neoexpert
 *
 */
public class User extends Node {

    private String password;

    /**
     *
     * @param user
     */
    public User(Node user) {
        super(user);
    }

    public User(JSONObject user,Controller c) {
        super(user,c);
    }

    /**
     *
     * @param userID
     * @param c
     */
    public User(ObjectID userID, Controller c) {
        super(userID, c);
    }

    /**
     *
     * @param group
     * @return
     */
    public boolean isInGroup(String group) {
        Object groups = doc.get("groups");
        if (groups == null) {
            return false;
        }
        return ((List) groups).contains(group);
    }

    /**
     *
     * @param as
     * @return
     */
    public boolean isLoggedIn(String as) {
        return isInGroup(as);
    }

    /**
     *
     * @return
     */
    public String getEmail() {
        return getName();
    }

    /**
     *
     * @param subject
     * @param text
     * @param mailsettings
     * @throws Exception
     */
    public void sendMail(String subject, String text, Node mailsettings) throws Exception {
        //SendEmail sm = new SendEmail(mailsettings);
        //sm.sendEmail(getEmail(), subject, text);
    }

    /**
     *
     * @param newPassword
     * @param u
     */
    public void setPassword(String newPassword, User u) {
        setAttribute("password", newPassword).commit(u);
    }

    /**
     *
     * @param group
     * @param u
     */
    public void addToGroup(String group, User u) {
        addToSet("groups", group, u);
    }

    static {
        addChildType(User.class, User.class);
    }

    /**
     *
     * @return
     */
    public boolean isAdmin() {
        List groups = (List) doc.get("groups");
        if (groups == null) {
            return false;
        }
        return (groups).contains("admin");
    }

    /**
     *
     * @param groups
     * @return
     */
    public boolean isInGroup(List groups) {
        List<String> mygroups = (List) doc.get("groups");
        if (mygroups == null || groups == null) {
            return false;
        }
        for (String group : mygroups) {
            if (groups.contains(group)) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @return
     */
    @Override
    public boolean canInherit() {
        return false;
    }

    /**
     *
     * @return
     */
    public List<String> getGroups() {
        return (List<String>) doc.get("groups");
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password){
        this.password=password;
    }

}
