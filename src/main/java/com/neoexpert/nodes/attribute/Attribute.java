package com.neoexpert.nodes.attribute;

import com.neoexpert.id.ObjectID;
import com.neoexpert.nodes.Node;
import com.neoexpert.nodes.User;
import com.neoexpert.nodes.controller.Controller;
import com.neoexpert.nodes.types.Primitive;
import com.neoexpert.nodes.types.Types;
import java.util.Locale;

import org.json.JSONObject;


/**
 *
 * @author neoexpert
 */
public class Attribute {

    private final Node parent;
    private final String name;
    private final Object value;
    private final Controller c;
    private boolean inherited;

    /**
     *
     * @param name
     * @param parent
     * @param c
     */
    public Attribute(String name, Node parent, Controller c) {
        this.name = name;
        this.parent = parent;
        value = parent.doc.get(name);
        this.c = c;
    }

    /**
     *
     * @return
     */
    public Object getValue() {
        return value;
    }

    /**
     *
     * @param lang
     * @return
     */
    public Object getValue(Locale lang) {
        if (lang == null) {
            return getValue();
        }
        if (value instanceof JSONObject) {
            Object v = ((JSONObject) value).get(lang.getLanguage());
            if (v == null) {
                v = ((JSONObject) value).get("de");
            }
            if (v == null) {
                return "";
            }
            return v;
        }
        if (value == null) {
            return "";
        }
        return value;
    }

    /**
     *
     * @param lang
     * @return
     */
    @SuppressWarnings("unchecked")
    public JSONObject toJSON(Locale lang) {
        JSONObject jo = new JSONObject();
        jo.put("parent", parent.getID().get());
        jo.put("name", name);

        Types types;
        types = (Types) c.getTypes();
        Primitive type = (Primitive)types.getChild(name);

        if (type == null) {
            jo.put("value", getValue());
        } else {
            jo.put("value", type.valueOf(this, lang));

            jo.put("type", type.getInfo().put("caption", type.getCaption(lang)));
        }
        return jo;
    }

    /**
     *
     * @param lang
     * @return
     */
    public JSONObject toJsonAdmin(Locale lang) {
        JSONObject jo = new JSONObject();
        jo.put("parent", parent.getID());
        jo.put("name", name);
        jo.put("inherited", inherited);

        if (name.equals("URL")) {
            getClass();
        }

        Primitive type = getType();

        if (type == null) {
            jo.put("value", getValue());
        } else {
            jo.put("value", type.valueOf(this, lang));

            jo.put("type", type.getAdminInfo().put("caption", type.getCaption(lang)));
        }
        return jo;
    }

    /**
     *
     * @return
     */
    @SuppressWarnings("unchecked")

    public Primitive getType() {
        Types types = c.getTypes();
        if (name.startsWith("_")) {
            return (Primitive) types.getChild(name.substring(0, name.indexOf('_')));
        } else {
            return (Primitive) types.getChild(name);
        }
    }

    /**
     *
     * @return
     */
    public Attribute setInherited() {
        inherited = true;
        return this;
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param locale
     * @return
     */
    public String toHTML(Locale locale) {
        return getValue(locale).toString();
    }

    /**
     *
     * @return
     */
    public boolean multiLang() {
        return false;
    }

    /**
     *
     * @return
     */
    public boolean hasParent() {
        return false;
    }

    /**
     *
     * @return
     */
    public int getOrderID() {
        return 0;
    }

    /**
     *
     * @param lang
     * @return
     */
    public String getCaption(Locale lang) {
        return name;
    }

    /**
     *
     * @return
     */
    public ObjectID getParentID() {
        return parent.getID();
    }

    /**
     *
     * @return
     */
    public boolean isConfigurable() {
        // TODO Auto-generated method stub
        return false;
    }

    /**
     *
     * @param confs
     */
    public void setConfiguration(JSONObject confs) {
        // TODO Auto-generated method stub

    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return name;
    }

    /**
     *
     * @param value
     * @param locale
     * @param u
     */
    public void setValue(String value, Locale locale, User u) {
        Primitive type = getType();
        if (type == null) {
            parent.doc.put(name, value);
            parent.commit(u);
        } else {
            type.setValueOf(this, value, locale, u);
        }
    }

    /**
     *
     * @return
     */
    public Node getParent() {
        return parent;
    }

    /**
     *
     * @return
     */
    public boolean isMultilang() {
        return getType().isMultilang();
    }

}
