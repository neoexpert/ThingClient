package com.neoexpert.nodes;
import com.neoexpert.id.ObjectID;


import com.neoexpert.nodes.controller.Controller;


/**
 *
 * @author neoexpert
 */
public class Category extends Node {
	static{
		addChildType(Category.class,Category.class);
		addChildType(Category.class,File.class);

	}

    /**
     *
     * @param c
     */
    public Category(Node c) {
		super(c);
	}

    /**
     *
     * @param i
     * @param c
     */
    public Category(ObjectID i, Controller c) {
		super(i,c);
	}
}
