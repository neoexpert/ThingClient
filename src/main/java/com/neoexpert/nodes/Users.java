package com.neoexpert.nodes;
import com.neoexpert.id.ObjectID;


import com.neoexpert.nodes.controller.Controller;
import com.neoexpert.nodes.templates.Template;

/**
 *
 * @author neoexpert
 */
public class Users extends Node {
	static{
		addChildType(Users.class,User.class);
		addChildType(Users.class,Template.class);

	}

    /**
     *
     * @param n
     */
    public Users(Node n) {
		super(n);
	}

    /**
     *
     * @param i
     * @param c
     */
    public Users(ObjectID i, Controller c) {
		super(i,c);
	}
	
	
}
