/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neoexpert.nodes;

import com.neoexpert.nodes.attribute.Attribute;


/**
 *
 * @author neoexpert
 */
public class Settings extends Node {

    static {
        addChildType(Domain.class, Settings.class);
        addChildType(Settings.class, Node.class);

    }

    public Settings(Node n) {
        super(n);
    }

    public String getServerName() {
        Attribute server_name = getAttribute("server_name");
        if (server_name != null) {
            return (String) server_name.getValue();
        }
        return null;
    }

}
