package com.neoexpert.nodes.types;
import com.neoexpert.id.ObjectID;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import com.neoexpert.nodes.Node;
import com.neoexpert.nodes.templates.Template;
import com.neoexpert.nodes.attribute.Attribute;
import com.neoexpert.nodes.controller.Controller;

/**
 *
 * @author neoexpert
 */
public class Select extends Primitive {

    /**
     *
     */
    public static final int TYPE_SELECT = 6;

    /**
     *
     * @param userID
     * @param con
     */
    public Select(ObjectID userID, Controller con) {
		super(userID, con);
	}

    /**
     *
     * @param n
     */
    public Select(Node n) {
		super(n);
	}

    /**
     *
     * @return
     */
    @Override
	public JSONObject getInfo() {
		JSONObject jo = new JSONObject();
		Template t = (Template) getChild("template");
		if (t != null)
			jo.put("template", t.getValue());
		t = (Template) getChild("element_template");
		if (t != null)
			jo.put("element_template", t.getValue());

		jo.put("multilang", isMultilang());
		jo.put("value_type", getValueType());

		return jo;
	}

    /**
     *
     * @return
     */
    @Override
    public JSONObject getAdminInfo() {
		JSONObject jo = new JSONObject();
		Template t = (Template) getChild("admin_template");
		if (t != null)
			jo.put("template", t.getValue());
		t = (Template) getChild("element_template");
		if (t != null)
			jo.put("element_template", t.getValue());

		jo.put("multilang", isMultilang());
		jo.put("value_type", getValueType());
		return jo;
	}
	
    /**
     *
     * @param a
     * @param lang
     * @return
     */
    @Override
	public Object valueOf(Attribute a, Locale lang) {
		ArrayList<?> d;
		JSONArray val = new JSONArray();

		if (isMultilang()) {
			d = (ArrayList<?>) a.getValue();
			for (Object v : d) {
				JSONObject entry = (JSONObject) v;
				Set<String> keys = entry.keySet();
				String key = keys.iterator().next();
				// val.put(key, ((Document)entry.get(key)).get(lang.getLanguage()));
				val.put(new JSONObject().put(key, ((JSONObject) entry.get(key)).get(lang.getLanguage())));
			}
		} else {
			d = (ArrayList<?>) a.getValue();
			for (Object v : d) {
				JSONObject entry = (JSONObject) v;
				Set<String> keys = entry.keySet();
				String key = keys.iterator().next();
				// val.put(key, ((Document)entry.get(key)).get(lang.getLanguage()));
				val.put(new JSONObject().put(key, ((JSONObject) entry.get(key))));
			}
		}

		return val;
	}

}
