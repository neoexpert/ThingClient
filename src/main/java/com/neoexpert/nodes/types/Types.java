package com.neoexpert.nodes.types;
import com.neoexpert.id.ObjectID;

import com.neoexpert.nodes.Node;
import com.neoexpert.nodes.controller.Controller;

/**
 *
 * @author neoexpert
 */
public class Types extends Node {
	static {
		addChildType(Types.class, Primitive.class);
		addChildType(Types.class, Collection.class);
	}

    /**
     *
     * @param category
     */
    public Types(Node category) {
		super(category);
	}

    /**
     *
     * @param userID
     * @param con
     */
    public Types(ObjectID userID, Controller con) {
		super(userID, con);
	}
}
