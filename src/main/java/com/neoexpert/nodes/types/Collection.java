package com.neoexpert.nodes.types;
import com.neoexpert.id.ObjectID;

import com.neoexpert.nodes.Node;
import com.neoexpert.nodes.controller.Controller;
import com.neoexpert.nodes.templates.Template;
import org.json.JSONObject;

/**
 *
 * @author neoexpert
 */
public class Collection extends Primitive {

    /**
     *
     */
    public static final int TYPE_COLLECTION = 4;

    /**
     *
     * @param category
     */
    public Collection(Node category) {
        super(category);
    }

    /**
     *
     * @param userID
     * @param con
     */
    public Collection(ObjectID userID, Controller con) {
        super(userID, con);
    }

  

    /**
     *
     * @return
     */
    @Override
    public int getValueType() {
        return TYPE_COLLECTION;
    }

    /**
     *
     * @return
     */
    public int getElementValueType() {
        if (doc.has("value_type")) {
            return doc.getInt("value_type");
        }
        return TYPE_STRING;
    }

    /**
     *
     * @return
     */
    @Override
    public JSONObject getInfo() {
        JSONObject jo = new JSONObject();
        Template t = (Template) getChild("template");
        if (t != null) {
            jo.put("template", t.getValue());
        }
        t = (Template) getChild("element_template");
        if (t != null) {
            jo.put("element_template", t.getValue());
        }

        jo.put("multilang", isMultilang());
        jo.put("value_type", getValueType());
        return jo;
    }
}
