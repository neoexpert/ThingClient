package com.neoexpert.nodes;

import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;


/**
 *
 * @author neoexpert
 */
public class File extends Node {

    /**
     *
     * @param c
     */
    public File(Node c) {
		super(c);
	}
	
    /**
     *
     * @return
     */
    @Override
	public boolean canInherit() {
		// TODO Auto-generated method stub
		return false;
	}
	
   
	
    /**
     *
     * @param u
     */
    @Override
	public void onUpdate(User u) {
		

		super.onUpdate(u);
	}
	
    /**
     *
     * @return
     */
    public String getContent() {
		Object m = doc.get("minified");
		if(m==null)
		return doc.getString("value");
		else
			return m.toString();
	}
	
   
}
