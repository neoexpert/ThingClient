package com.neoexpert.nodes.templates;
import com.neoexpert.id.ObjectID;

import java.util.Locale;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.neoexpert.nodes.Node;
import com.neoexpert.nodes.User;
import com.neoexpert.nodes.attribute.Attribute;
import com.neoexpert.nodes.controller.Controller;

/**
 *
 * @author neoexpert
 */
public class StringTemplate extends Template {

	static {
	}

    /**
     *
     * @param cat
     */
    public StringTemplate(Node cat) {
		super(cat);
	}

    /**
     *
     * @param userID
     * @param con
     */
    public StringTemplate(ObjectID userID, Controller con) {
		super(userID, con);
	}

    /**
     *
     * @param n
     * @param locale
     * @return
     */
    @Override
	public String fill(Node n, Locale locale) {
		String s = getValue();
		Pattern pattern = Pattern.compile("\\{\\{(.*?)\\}\\}");
		Matcher matcher = pattern.matcher(s);
		while (matcher.find()) {
			String group = matcher.group();
			group = group.substring(2, group.length() - 2);
			Attribute attr = getAttribute(group);
			if (attr != null)
				s = s.replaceAll(Pattern.quote("{{" + group + "}}"), attr.toHTML(locale));
		}

		return s;
	}

    /**
     *
     * @return
     */
    @Override
	public boolean canInherit() {
		return false;
	}

    /**
     *
     * @param u
     */
    @Override
	public void onCreate(User u) {
		addAttr("value", u);
	}

    /**
     *
     * @param v
     * @return
     */
    public Node setValue(String v) {
		doc.put("value", v);
		return this;
	}

}
