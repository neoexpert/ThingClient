package com.neoexpert.nodes.templates;
import com.neoexpert.Log;
import com.neoexpert.id.ObjectID;

import com.neoexpert.nodes.Node;
import com.neoexpert.nodes.attribute.Attribute;
import com.neoexpert.nodes.controller.Controller;
import java.util.Locale;

/**
 *
 * @author neoexpert
 */
public abstract class Template extends Node {

    /**
     *
     * @param cat
     */
    public Template(Node cat) {
        super(cat);
    }

    /**
     *
     * @param userID
     * @param con
     */
    public Template(ObjectID userID, Controller con) {
        super(userID, con);
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        try {
            return getValue();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.error(e);
        }
        return super.toString();
    }

    /**
     *
     * @return
     */
    public String getWrapperTag() {
        Attribute w = getAttribute("wrapper");
        if (w != null) {
            return w.getValue().toString();
        }
        return "div";
    }

    /**
     *
     * @return
     */
    public String getValue() {
        return doc.getString("value");
    }

    /**
     *
     * @param n
     * @param locale
     * @return
     */
    public abstract String fill(Node n, Locale locale);
}
