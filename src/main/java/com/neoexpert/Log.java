package com.neoexpert;
import com.neoexpert.nodes.*;

public class Log {
	private Log(){}
	static{
		log("thing started");
	}
	public static void ok(){
		System.out.println("[ "+green("OK")+" ]");
	}

	public static void warn(String s) {
		System.out.print("["+yellow("INFO")+"] ");

		System.out.println(s);
		System.out.flush();
	}

	public static void info(String s) {
		System.out.print("["+blue("INFO")+"] ");

		System.out.println(s);
		System.out.flush();
	}
	public static final int DEBUG=3;
	public static final int INFO=2;
	public static final int WARN=1;
	public static final int ERROR=0;
	public static int LOG_LEVEL=ERROR;

	public static void log(String s,int LEVEL) {
		if(LEVEL<=LOG_LEVEL)
			log(s);

	}
	public static void log(Object o) {
					log(o.toString());
	}
	public static void log(String s) {
		System.out.print("["+blue("THING")+"] ");
		System.out.println(s);
		System.out.flush();
	}
	public static void logYellow(Object o) {
		System.out.print("["+blue("THING")+"] ");
		System.out.println(yellow(o.toString()));
		System.out.flush();
	}
	public static void logGreen(Object o) {
		System.out.print("["+blue("THING")+"] ");
		System.out.println(green(o.toString()));
		System.out.flush();
	}
	public static void logRed(Object o) {
		System.out.print("["+blue("THING")+"] ");
		System.out.println(red(o.toString()));
		System.out.flush();
	}
	public static void log(User u,String s) {
		if(u!=null)
			System.out.print("["+cyan(u.getID().toString())+"] ");

		else
			System.out.print("["+blue("nobody")+"] ");
		System.out.println(s);
		System.out.flush();
	}

	public static void error(String s) {
		System.out.print("\007");
		System.out.print("["+red("ERROR")+"] ");

		System.err.println(s);
		System.err.flush();
	}

	public static void error(Throwable e) {
		System.err.print("\007");
		System.err.print("["+red("ERROR")+"] ");

		e.printStackTrace(System.err);
		System.err.flush();
	}
	public static void error(User u,String e) {
		if(u!=null)
			System.err.print("["+red(u.getID().toString())+"] ");

		else
			System.err.print("["+red("nobody")+"] ");
		System.err.println(e);
		System.err.flush();
	}

	private static String red(String s) {
		return "\033[31;1m" + s + "\033[0m";
	}

	private static String green(String s) {
		return "\033[32;1m" + s + "\033[0m";
	}

	private static String yellow(String s) {
		return "\033[93;1m" + s + "\033[0m";
	}
	private static String blue(String s) {
		return "\033[34;1m" + s + "\033[0m";
	}
	private static String magenta(String s) {
		return "\033[45;1m" + s + "\033[0m";
	}
	private static String cyan(String s) {
		return "\033[36;1m" + s + "\033[0m";
	}
}
